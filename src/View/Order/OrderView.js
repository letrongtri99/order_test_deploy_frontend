import React, { useState, useEffect } from "react";
import LayoutAll from "../Layout";
import {
  Layout,
  Tooltip,
  Button,
  Card,
  Modal,
  Row,
  Col,
  Image,
  InputNumber,
  Typography,
  Spin,
  List,
  Popconfirm,
  message,
} from "antd";
import ItemsCarousel from "react-items-carousel";
import {
  LeftOutlined,
  RightOutlined,
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
  ShoppingCartOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import io from "socket.io-client";
import "./OrderView.css";
import Config from "../../config";

const { Header, Content, Footer, Sider } = Layout;
const { Meta } = Card;
const { Title, Text } = Typography;
const { confirm } = Modal;

let socket;

const OrderView = () => {
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [listFood, setListFood] = useState([]);
  const [name, setName] = useState("");
  const [imgUrl, setImgUrl] = useState("");
  const [listOrder, setListOrder] = useState([]);
  const [stateCreate, setStateCreate] = useState({
    id: "",
    count: 1,
  });
  const chevronWidth = 40;
  const ENDPOINT = `${Config.chatbot}`;
  socket = io(ENDPOINT);

  const showModal = (id) => {
    fetch(`${Config.developer}/food/getById?id=${id}`, {
      method: "GET",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setStateCreate({
          id: data.data._id,
          count: 1,
        });
        setName(data.data.name);
        setImgUrl(data.data.imageUrl);
        setVisible(true);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: "Bạn có chắc chắn muốn hủy order này?",
      icon: <ExclamationCircleOutlined />,
      okText: "Đồng ý",
      okType: "danger",
      cancelText: "Từ chối",
      onOk() {
        fetch(`${Config.developer}/order/updateDelete`, {
          method: "POST",
          credentials: "include",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: id,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            message.success("Hủy thành công");
            setLoading(true);
            socket.emit("order");
          })
          .catch((e) => {
            console.log(e);
          });
      },
      onCancel() {},
    });
  };

  const handleOk = () => {
    setConfirmLoading(true);
    fetch(`${Config.developer}/order/create`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(stateCreate),
    })
      .then((res) => res.json())
      .then((data) => {
        setVisible(false);
        setConfirmLoading(false);
        message.success("Tạo order thành công");
        setLoading(true);
        socket.emit("order");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleCancel = () => {
    setVisible(false);
  };

  useEffect(() => {
    getDataByCategory();
    socket.emit("join");
    socket.on("response", (res) => {
      getAllOrder();
    });
  }, []);

  const getAllOrder = () => {
    setLoading(true);
    fetch(`${Config.developer}/order/getAllOrder`, {
      method: "GET",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const listPush = [];
        data.data.forEach((e) => {
          listPush.push({
            id: e._id,
            name: e.food_id.name,
            imageUrl: e.food_id.imageUrl,
            count: e.count,
            status: e.status,
          });
        });
        setListOrder(listPush);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const confirmFinish = (id) => {
    console.log(id);
    fetch(`${Config.developer}/order/updateDelete`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: id,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        message.success("Hoàn thành order");
        setLoading(true);
        socket.emit("order");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getDataByCategory = () => {
    fetch(`${Config.developer}/food/getAllData`, {
      method: "GET",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setListFood(data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <LayoutAll>
      <Content
        style={{
          padding: 24,
          margin: 0,
          minHeight: "110vh",
          background: "#fff",
        }}
      >
        <Modal
          title="Xác nhận đặt đồ"
          visible={visible}
          onOk={handleOk}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
          okText="Đặt"
          cancelText="Hủy"
        >
          <Row>
            <Col span={12}>
              <Image width={200} height={110} src={imgUrl} />
            </Col>
            <Col span={12}>
              <Row>
                <Col span={24}>
                  <Title level={5}>{name}</Title>
                </Col>
              </Row>
              <Row>
                <Col span={16}>
                  <Text
                    style={{
                      fontSize: "15px",
                      verticalAlign: "middle",
                      lineHeight: "normal",
                    }}
                  >
                    Số lượng:
                  </Text>
                </Col>
                <Col span={8}>
                  <InputNumber
                    min={1}
                    max={10}
                    value={stateCreate.count}
                    onChange={(value) => {
                      setStateCreate({
                        ...stateCreate,
                        count: value,
                      });
                    }}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Modal>
        <div style={{ padding: `0 ${chevronWidth}px` }}>
          <h2>1. Món bò</h2>
          <ItemsCarousel
            requestToChangeActive={setActiveItemIndex}
            activeItemIndex={activeItemIndex}
            numberOfCards={4}
            gutter={20}
            leftChevron={
              <Tooltip title="search">
                <Button shape="circle" icon={<LeftOutlined />} />
              </Tooltip>
            }
            rightChevron={
              <Tooltip title="search">
                <Button shape="circle" icon={<RightOutlined />} />
              </Tooltip>
            }
            outsideChevron
            chevronWidth={chevronWidth}
          >
            {listFood.map((e) => {
              if (e.category == "MONBO") {
                return (
                  <Card
                    style={{ width: 250, height: 260 }}
                    cover={<img alt="example" src={e.imageUrl} height={160} />}
                    actions={[
                      <ShoppingCartOutlined
                        onClick={() => {
                          showModal(e._id);
                        }}
                        key="setting"
                        style={{ fontSize: "20px" }}
                      />,
                    ]}
                  >
                    <Meta title={e.name} style={{ height: 1 }} />
                  </Card>
                );
              }
            })}
          </ItemsCarousel>
          <h2>2. Món gà</h2>
          <ItemsCarousel
            requestToChangeActive={setActiveItemIndex}
            activeItemIndex={activeItemIndex}
            numberOfCards={4}
            gutter={20}
            leftChevron={
              <Tooltip title="search">
                <Button shape="circle" icon={<LeftOutlined />} />
              </Tooltip>
            }
            rightChevron={
              <Tooltip title="search">
                <Button shape="circle" icon={<RightOutlined />} />
              </Tooltip>
            }
            outsideChevron
            chevronWidth={chevronWidth}
          >
            {listFood.map((e) => {
              if (e.category == "MONGA") {
                return (
                  <Card
                    style={{ width: 250, height: 260 }}
                    cover={<img alt="example" src={e.imageUrl} height={160} />}
                    actions={[
                      <ShoppingCartOutlined
                        onClick={() => {
                          showModal(e._id);
                        }}
                        key="setting"
                        style={{ fontSize: "20px" }}
                      />,
                    ]}
                  >
                    <Meta title={e.name} style={{ height: 1 }} />
                  </Card>
                );
              }
            })}
          </ItemsCarousel>
        </div>
      </Content>
      <Sider
        style={{ backgroundColor: "#fff", borderLeft: "0.6px solid #ebe6e6" }}
        width={400}
      >
        <Title level={3} style={{ margin: "0px", paddingLeft: "13px" }}>
          Danh sách order
        </Title>
        {loading == true ? (
          <div className="demo-loading-container">
            <Spin style={{ fontSize: "55px" }} />
          </div>
        ) : (
          <List
            dataSource={listOrder}
            renderItem={(item) => (
              <List.Item>
                <Row style={{ padding: "15px", width: "100%" }}>
                  <Col span={12}>
                    <img width={160} height={100} src={item.imageUrl} />
                  </Col>
                  <Col span={12}>
                    <Row>
                      <Col span={24}>
                        <Title level={5}>
                          {item.count} {item.name}
                        </Title>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={24}>
                        <Text>
                          Trạng thái:
                          {item.status == 0
                            ? " Chờ xác nhận"
                            : item.status == 1
                            ? " Đầu bếp đang chế biến"
                            : item.status == 2
                            ? " Đầu bếp từ chối"
                            : item.status == 3
                            ? " Đầu bếp hoàn thành"
                            : item.status == 4
                            ? " Hết nguyên liệu"
                            : ""}
                        </Text>
                      </Col>
                    </Row>
                    <Row style={{ marginTop: "10px" }}>
                      <Col span={24}>
                        {item.status == 0 ||
                        item.status == 2 ||
                        item.status == 4 ? (
                          <Button
                            type="primary"
                            danger
                            size="small"
                            style={{ float: "right", marginRight: "10px" }}
                            onClick={() => {
                              showDeleteConfirm(item.id);
                            }}
                          >
                            Hủy
                          </Button>
                        ) : item.status == 3 ? (
                          <Button
                            type="primary"
                            size="small"
                            style={{ float: "right", marginRight: "10px" }}
                            onClick={() => {
                              confirmFinish(item.id);
                            }}
                          >
                            Phục vụ
                          </Button>
                        ) : (
                          <div></div>
                        )}
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </List.Item>
            )}
          ></List>
        )}
      </Sider>
    </LayoutAll>
  );
};

export default OrderView;
