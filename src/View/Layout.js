import React, { useState } from "react";
import { Layout, Menu, Button, message } from "antd";
import { Switch, Route, Link, useHistory, useLocation } from "react-router-dom";
import { LogoutOutlined } from "@ant-design/icons";
import Config from "../config";
import "./Layout.css";

const { Header, Footer, Sider, Content } = Layout;

const LayoutAll = (props) => {
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const location = useLocation();

  const activeClass = (route) => {
    if (location.pathname == route) {
      return "activeMenu";
    } else {
      return "inactiveMenu";
    }
  };
  const logOut = () => {
    setLoading(true);
    fetch(`${Config.developer}/user/logout`, {
      method: "GET",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.success == true) {
          history.push("/");
        } else {
          message.error("Không thành công");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };
  return (
    <Layout className="layout">
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item
            key="1"
            onClick={() => {
              history.push("/order");
            }}
            className={activeClass("/order")}
          >
            Order
          </Menu.Item>
          <Menu.Item
            key="2"
            onClick={() => {
              history.push("/kitchen");
            }}
            className={activeClass("/kitchen")}
          >
            Đầu bếp
          </Menu.Item>
          <Button
            type="primary"
            shape="circle"
            icon={<LogoutOutlined />}
            className="logout"
            size="large"
            onClick={logOut}
            loading={loading}
          />
        </Menu>
      </Header>
      <Layout>{props.children}</Layout>
      <Footer
        style={{
          height: "75px",
          position: "fixed",
          bottom: 0,
          width: "100%",
          marginTop: "5px",
        }}
      >
        © Copyright by LeTrongTri
      </Footer>
    </Layout>
  );
};

export default LayoutAll;
