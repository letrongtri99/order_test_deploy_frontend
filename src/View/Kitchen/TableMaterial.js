import React, { useState, useEffect } from "react";
import {
  Table,
  Tag,
  Space,
  Button,
  Modal,
  Row,
  Col,
  Input,
  Select,
  InputNumber,
  message,
} from "antd";

import { EditOutlined, PlusOutlined } from "@ant-design/icons";
import io from "socket.io-client";
import Config from "../../config";

const socket = io(`${Config.chatbot}`);

const options = [
  {
    label: "Thịt",
    value: "THIT",
  },
  {
    label: "Ăn kèm",
    value: "ANKEM",
  },
  {
    label: "Rau",
    value: "RAU",
  },
];

const { Option } = Select;

const TableMaterial = () => {
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 15,
    total: 0,
  });
  const [listMaterials, setListMaterials] = useState([]);
  const [loadTable, setLoadTable] = useState(true);
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [valueSelect, setValueSelect] = useState("THIT");
  const [statusChange, setStatusChange] = useState(0);
  const [state, setState] = useState({
    _id: "",
    name: "",
    category: "THIT",
    count: 0,
  });
  const columns = [
    {
      title: "Tên",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Số lượng",
      dataIndex: "count",
      key: "count",
      render: (count, record) => {
        return (
          <div>
            {count} {record.unit}
          </div>
        );
      },
    },
    {
      title: "Phân loại",
      key: "category",
      dataIndex: "category",
      render: (category) => {
        let color = "";
        let text = "";
        switch (category) {
          case "THIT":
            color = "geekblue";
            text = "Thịt";
            break;
          case "ANKEM":
            color = "green";
            text = "Ăn kèm";
            break;
          case "RAU":
            color = "volcano";
            text = "Rau";
            break;
          default:
            break;
        }
        return (
          <Tag key={category} color={color}>
            {text.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            type="primary"
            shape="circle"
            icon={<EditOutlined />}
            onClick={() => {
              showModal(record._id);
            }}
          />
        </Space>
      ),
    },
  ];
  const handleTableChange = (pagination, filters, sorter) => {
    setPagination(pagination);
  };

  const getAllMaterials = () => {
    fetch(`${Config.developer}/ingredients/getIngredientsPagintion`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        limit: pagination.pageSize,
        skip: pagination.current,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setLoadTable(false);
        setListMaterials(data.data);
        setPagination({
          ...pagination,
          total: data.count,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const showModal = (id) => {
    setState({
      _id: "",
      name: "",
      category: "THIT",
      count: 0,
    });
    fetch(`${Config.developer}/ingredients/getById?id=${id}`, {
      method: "GET",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setState({
          _id: data.data._id,
          name: data.data.name,
          category: data.data.category,
          count: data.data.count,
        });
        setVisible(true);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleOk = () => {
    if (state._id != "") {
      fetch(`${Config.developer}/ingredients/update`, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(state),
      })
        .then((res) => res.json())
        .then((data) => {
          setVisible(false);
          message.success("Cập nhật thành công");
          setStatusChange(Math.random() * 10);
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      fetch(`${Config.developer}/ingredients/create`, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: state.name,
          category: state.category,
          count: state.count,
          unit: "kg",
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setVisible(false);
          message.success("Thêm mới thành công");
          setStatusChange(Math.random() * 10);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const showModalCreate = () => {
    setState({
      _id: "",
      name: "",
      category: "THIT",
      count: 0,
    });
    setVisible(true);
  };

  useEffect(() => {
    socket.on("response", (res) => {
      getAllMaterials();
    });
  }, []);

  useEffect(() => {
    getAllMaterials();
  }, [pagination.current, statusChange]);

  return (
    <div>
      <Modal
        title={
          state._id != "" ? "Cập nhật nguyên liệu" : "Thêm mới nguyên liệu"
        }
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText={state._id != "" ? "Cập nhật" : "Thêm mới"}
        cancelText="Đóng"
      >
        <Row style={{ marginBottom: "15px" }}>
          <Col span={6} style={{ paddingTop: "2px" }}>
            Tên:
          </Col>
          <Col span={18}>
            <Input
              value={state.name}
              onChange={(e) => {
                console.log(e);
                setState({
                  ...state,
                  name: e.target.value,
                });
              }}
            />
          </Col>
        </Row>
        <Row style={{ marginBottom: "15px" }}>
          <Col span={6} style={{ paddingTop: "1%" }}>
            Số lượng:
          </Col>
          <Col span={18}>
            <InputNumber
              value={state.count}
              min={0}
              max={100}
              formatter={(value) => `${value}kg`}
              parser={(value) => value.replace("kg", "")}
              onChange={(value) => {
                setState({
                  ...state,
                  count: value,
                });
              }}
            />
          </Col>
        </Row>
        <Row>
          <Col span={6} style={{ paddingTop: "1%" }}>
            Phân loại:
          </Col>
          <Col span={18}>
            <Select
              value={state.category}
              style={{ width: "100%" }}
              onChange={(value) => {
                setState({
                  ...state,
                  category: value,
                });
              }}
            >
              {options.map((e) => (
                <Option value={e.value}>{e.label}</Option>
              ))}
            </Select>
          </Col>
        </Row>
      </Modal>
      <div style={{ width: "100%" }}>
        <Button
          type="primary"
          icon={<PlusOutlined />}
          style={{ float: "right" }}
          onClick={showModalCreate}
        >
          Thêm mới
        </Button>
      </div>
      <br />
      <br />
      <Table
        columns={columns}
        dataSource={listMaterials}
        pagination={pagination}
        onChange={handleTableChange}
        loading={loadTable}
      />
    </div>
  );
};

export default TableMaterial;
