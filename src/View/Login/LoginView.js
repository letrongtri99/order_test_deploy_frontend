import React, { useState, useEffect } from "react";
import { Form, Input, Button, Checkbox, message } from "antd";
import { useHistory } from "react-router-dom";
import Config from "../../config";
import "./LoginView.css";
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const Demo = () => {
  const [loadLogin, setLoadLogin] = useState(false);
  const history = useHistory();
  const onFinish = (values) => {
    setLoadLogin(true);
    fetch(`${Config.developer}/user/login`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    })
      .then((res) => res.json())
      .then((data) => {
        setLoadLogin(false);
        if (data.success == false) {
          message.error("Sai thông tin đăng nhập");
        } else {
          history.push("/order");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div
      style={{
        position: "relative",
        height: "100vh",
        backgroundImage:
          "url('https://i.pinimg.com/originals/e6/7d/af/e67daf68a6e8f6d4a9283cb7d64b098c.jpg')",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    >
      <Form
        {...layout}
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        style={{
          position: "absolute",
          top: "32%",
          left: "38%",
          boxShadow: "0 0 3px #b1ceac",
          padding: "25px",
          backgroundColor: "#dde5e3",
        }}
      >
        <h2 style={{ textAlign: "center", marginBottom: "15px" }}>
          Đăng nhập vào hệ thống
        </h2>
        <Form.Item
          label="Tên"
          name="email"
          rules={[
            {
              required: true,
              message: "Nhập đầy đủ tên!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mật khẩu"
          name="password"
          rules={[
            {
              required: true,
              message: "Nhập đầy đủ mật khẩu!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit" loading={loadLogin}>
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Demo;
